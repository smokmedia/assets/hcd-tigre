---
title: Sandra Rossi
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: images/concejales/Sandra-Rossi.jpg
cargo: Prosecretaria del Honorable Concejo Deliberante de Tigre
rol: prosec
rolp: concejal
contact:
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/sberossi
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/rossisandrab
---
### Información Personal