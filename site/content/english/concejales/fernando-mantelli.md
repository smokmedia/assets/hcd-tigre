---
title: Fernando Mantelli
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/f-mantelli.jpg"
cargop: 'Vicepresidente del Bloque de Frente'
rol: vice1
rolp: Vice
contact:
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/MantelliFOficia
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/fernando.d.mantelli

cargo: Vice Presidente 1° del Honorable Concejo Deliberante de Tigre

---
### Información Personal