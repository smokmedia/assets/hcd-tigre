+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
cargop = "Secretario del Bloque Frente de Todos"
control = "frente"
image = "/images/photo-2022-03-09-19-52-52.jpg"
rol = "concejal"
rolp = "Secretario"
title = "Francisco Fernández"
cargo = "Concejal"
[[contact]]
icon = ""
link = "https://www.instagram.com/totofernandezmiranda/"
name = "Instagram"

+++
Estoy casado con mi novia de toda la vida, la conocí cuando tenía 14 años y ella 12. Tengo cuatro hijos: Iñaki es el más grande con 14, Joaquina de 12, Carola de 10 y Rufino que va a cumplir 8. Nací y crecí en Torcuato, vine a vivir cuando tenía tres años. Amo mi ciudad. Es el lugar en donde me gusta estar y quiero crecer, y donde quiero aportar algo desde el lugar que me toque hoy siendo Concejal.

Creo que, desde los consensos, la armonía y la alegría, es donde las cosas se generan y desde donde se pueden generar los cambios