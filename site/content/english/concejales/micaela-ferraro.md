---
title: "Micaela Ferraro"
control: "frente"
bg_image: "images/backgrounds/Recinto2-Recuperado.jpg"
image: "images/concejales/Micaela-Ferraro.jpeg"
cargo : "Concejal"
rol: "concejal"
rolp: "concejal"
contact:
  - name : "Twitter"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://www.twitter.com/micaferrarom"

  - name : "Instagram"
    icon : "ti-instagram" # icon pack : https://themify.me/themify-icons
    link : "https://www.instagram.com/micaferrarom"

---

### Informacion Personal

