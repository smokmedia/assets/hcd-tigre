+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargop = ""
contact = []
control = ""
description = ""
image = "/images/img_7792.jpg"
rol = "secretario"
rolp = ""
tipo = ""
title = "Leonardo Güi"
type = ""
[[cargos]]
cargo = "Secretario del Honorable Concejo Deliberante"

+++
#### Información

Leonardo Abel Güi es abogado y procurador por la Universidad Nacional de La Plata.

Desempeñó tareas como becario de investigación en la Honorable Cámara de Diputados de la Provincia de Buenos Aires, y como asesor en la Secretaría General de Gobierno de la Provincia de Buenos Aires.

Actualmente ejerce la docencia universitaria como profesor adjunto de la materia Derechos Humanos desde la Perspectiva Internacional en la Universidad de Ciencias Empresariales y Sociales, ocupando cargos en las sedes Tigre, San Isidro y CABA. A su vez se desempeña como auxiliar docente de primera en la materia Instituciones de Derecho Público de la Universidad de Buenos Aires, sede Pilar.