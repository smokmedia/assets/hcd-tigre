+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
cargop = "Concejal"
control = "juntos"
image = "/images/ll.jpg"
rol = "concejal"
rolp = "concejal"
title = "Lisandro Lanzón"
cargo = "Concejal"
[[contact]]
icon = "ti-facebook"
link = "https://www.instagram.com/lisandro.lanzon/"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/lisandro.lanzon/"
name = "Instagram"

+++
### Información

Lisandro Lanzón cursó estudios de Comunicación en la Universidad de Buenos Aires y actualmente cursa estudios de posgrado en la Maestría de Comunicación Política de la Escuela de Posgrado de la Facultad de Comunicación de la Universidad Austral.

En su experiencia profesional se desempañó como consultor en Asuntos Públicos en la Argentina y el exterior para diversas compañías del sector privado y público. A su vez como especialista en comunicación de gobierno asesoró a distintos niveles de gobierno y participó en la estrategia y gestión de campañas electorales nacionales, provinciales y locales en Argentina y Latinoamérica.

Participó en la creación del Centro de Ciudades Inteligentes de la Universidad de Buenos Aires e impulsó diversos encuentros académicos y profesionales sobre innovación y tecnología para modelos de gestión Smart Cities

Fue coordinador editorial del Libro sobre modelos de gestión urbana y el análisis de caso: "Un nuevo Sueño Poner a Tigre en Acción" ISBN 978-987-1294-87-9 junto a Pedro Cernadas y Omar Quiroga.

Además es productor y realizador audiovisual al frente de productoras de contenidos para TV que le valieron obtener tres veces el premio martin Fierro junto a otras cinco nominaciones además de obtener en diversas ocaciones el Premio FUND-TV y reconocimientos internacionales como el Premio del Festival de Berlin para cine inmersivo en domos o planetarios con la Obra Tango 360º producida junto a Pipi Piazolla sobre la obra de Astor Piazolla.