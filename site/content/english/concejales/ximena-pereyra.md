+++
bg_image = "/images/xime.jpeg"
bio = ""
cargo = "Concejal"
cargop = "Concejala"
control = "juntos"
description = "Concejala"
image = "/images/xime-p.jpeg"
rol = "concejal"
rolp = "concejal"
tipo = "juntos"
title = "Ximena Pereyra"
type = ""
[[cargos]]
cargo = "Concejala"
[[contact]]
icon = ""
link = ""
name = "Reddit"
[[contact]]
icon = "ti-facebook"
link = "https://www.facebook.com/ximepereyraok"
name = "Facebook"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/ximepereyraok/"
name = "Instagram"

+++
#### Información