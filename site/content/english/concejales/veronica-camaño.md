---
title: Verónica Caamaño
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/v-caamano.jpeg"
cargo: Concejala
rol: concejal
rolp: Vice
contact:
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/caa_vero
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/veronicacaamano_
cargop: Vice Presidenta del Bloque Frente de Todos

---
### Información Personal