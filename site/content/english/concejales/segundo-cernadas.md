---
title: Segundo Cernadas
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: images/segundo.cernadas.jpg
cargo: Presidente del Honorable Concejo Deliberante de Tigre
cargop: Vicepresidente del Bloque de Juntos
rol: presidente
rolp: concejal
contact:
- name: Facebook
  icon: ti-facebook
  link: https://www.facebook.com/segundocernadas
- name: Twitter
  icon: ti-twitter-alt
  link: https://twitter.com/segundocernadas
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/segundo.cernadas/

---
### Información Personal

Pedro Segundo Cernadas dedicó gran parte de su vida a la actuación. También es emprendedor y productor de agricultura alternativa.

En 2014 incursiona en la política del Partido de Tigre. En 2015 es electo concejal por la coalición de Cambiemos. Ese mismo año renuncia a su banca para asumir como titular de ANSES Pacheco.

En las elecciones de 2017 encabeza la lista de Cambiemos, derrotando al partido oficialista del actual intendente Julio Zamora.

En las elecciones de 2019 se postula como intendente de Tigre por Juntos por el Cambio, logrando el 35% de los votos.

En las elecciones legislativas del 2021 encabezó la lista de concejales para el espacio de Juntos, obteniendo 39,61% y cosechando de esta manera 7 bancas dentro del Concejo Deliberante para su espacio.