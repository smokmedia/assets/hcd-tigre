---
title: Gisela Zamora
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/zamora.jpg"
cargo: Concejal
rol: concejal
rolp: Presidente
cargop: Presidenta del Bloque Frente de Todos
contact:
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/giselalzamora
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/giselazamoraok

---
### Informacion Personal