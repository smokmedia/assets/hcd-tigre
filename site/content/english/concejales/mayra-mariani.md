---
title: "Mayra Mariani"
control: "frente"
bg_image: "images/backgrounds/Recinto2-Recuperado.jpg"
image: "images/concejales/Mayra-Mariani.jpg"
cargo : "Concejal"
rol: "concejal"
rolp: "concejal"
contact:
  - name : "Facebook"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/mayralorenamariani"

  - name : "Twitter"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://www.twitter.com/mayramariani"

  - name : "Instagram"
    icon : "ti-instagram" # icon pack : https://themify.me/themify-icons
    link : "https://www.instagram.com/mayra_mariani"

---

### Informacion Personal

