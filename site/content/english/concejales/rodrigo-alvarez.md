---
title: Rodrigo Alvarez
control: frente
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/r-alvarez.jpeg"
cargo: Concejal
rol: concejal
rolp: concejal
contact:
- name: Twitter
  icon: ti-twitter-alt
  link: https://www.twitter.com/roalv55
- name: Instagram
  icon: ti-instagram
  link: https://www.instagram.com/rodrigo_alvarezok

---
### Información Personal