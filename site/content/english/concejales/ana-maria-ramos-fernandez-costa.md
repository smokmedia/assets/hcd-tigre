---
title: Ana María Ramos Fernándes Costa
control: juntos
bg_image: images/backgrounds/Recinto2-Recuperado.jpg
image: "/images/ana-maria-fernandez-costa-1-1.jpeg"
cargo: 
cargop: Presidenta del Bloque de Juntos
rol: concejal
rolp: Presidente


---
### Información Personal

Nació en Portugal y de pequeña vino a la Argentina, para radicarse en la ciudad de General Pacheco, dónde aún sigue viviendo.

Es docente, madre de 4 hijos.

Su vocación es la docencia y dedicó toda su vida al desarrollo de actividades en diferentes escuelas de la gestión pública y privada. Es catequista y forma parte de la comunidad de la parroquia Inmaculada Concepción.