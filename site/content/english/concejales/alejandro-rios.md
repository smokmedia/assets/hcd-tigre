+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
cargop = ""
control = "frente"
image = "/images/alejandro-rios.jpeg"
rol = "concejal"
rolp = "concejal"
title = "Alejandro Ríos"
cargo = "Concejal"
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/alejandroriospf/"
name = "Instagram"

+++
#### Información

Alejandro Ramón Ríos nació el 14 de diciembre de 1986 en San Fernando. Es oriundo de Troncos del Talar en Tigre y actualmente reside Benavidez. Está casado con Micaela.