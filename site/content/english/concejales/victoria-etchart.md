+++
bg_image = "/images/Comisiones/recinto2-fondo.jpg"
bio = ""
cargo = "Concejal"
cargop = ""
cargos = []
control = "frente"
description = "Concejala"
image = "/images/vicky-etchart.jpeg"
rol = "concejal"
rolp = "concejala"
tipo = ""
title = "Victoria Etchart"
type = ""
[[contact]]
icon = "ti-instagram"
link = "https://www.instagram.com/victoriaetchart/"
name = "Instagram"

+++
#### Información