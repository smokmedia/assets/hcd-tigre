+++
author = ""
bg_image = "/images/280197288_363525079146146_6245011871341046623_n.jpg"
categories = []
date = 2022-05-10T03:00:00Z
description = ""
image = "/images/280197288_363525079146146_6245011871341046623_n-1.jpg"
tags = []
title = "INGRESO AL CONCEJO DELIBERANTE EL PROYECTO QUE PROPONE “FICHA LIMPIA” EN EL DISTRITO"
type = "post"

+++
_En la cuarta sesión ordinaria del Concejo deliberante se ingresaron y giraron a las comisiones de trabajo del cuerpo, veinte nuevos proyectos propuestos por los distintos bloques. El proyecto de “Ficha Limpia” impulsado por el Boque de Juntos se debatirá en la comisión de Legislación, interpretación y reglamento._

Durante la sesión y en la media hora de consultas y homenajes, la concejala Gisela Zamora recordó a la Dra. María Alejandra Nardi de quien se cumplía un nuevo aniversario de su natalicio “Fue una gran compañera, que siempre trabajó por los vecinos de Tigre, fue concejala y presidenta de este cuerpo y creo que es muy importante destacarla por su tenacidad y abnegado trabajo por el crecimiento de nuestra ciudad”.

Desde el Boque de Juntos la Concejala Ana M. Fernández Costa realizó un homenaje a los trabajadores y en especial mencionó a aquellos hombres y mujeres que trabajan en el Municipio, dando servicios a nuestros vecinos. También el Concejal Juan M. Furnari realizó un homenaje a los fallecidos en el Crucero ARA Gral. Belgrano en ocasión de cumplirse cuarenta años, recordó en especial a dos vecinos de Tigre fallecidos en el hundimiento.

Finalmente se aprobaron 17 iniciativas en beneficio de los vecinos del distrito, entre las que se encuentran instalaciones de Cámaras de seguridad en distintos barrios, la ampliación del recorrido del colectivo 21 en la localidad de Benavidez, la creación de una nueva feria artesanal en la localidad de Don Torcuato y una iniciativa convalidando un convenio con el Ministerio de Infraestructura y Servicios Públicos de la Provincia de Bs. As. Para la provisión luminarias Leds.