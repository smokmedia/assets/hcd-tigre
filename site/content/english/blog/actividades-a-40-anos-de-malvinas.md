+++
author = ""
bg_image = "/images/malvinas.jpg"
categories = []
date = 2022-04-06T03:00:00Z
description = ""
image = "/images/malvinas-1.jpg"
tags = []
title = "Actividades a 40 años de Malvinas"
type = "post"

+++
_En el marco de las actividades de conmemoración de los 40 años de la guerra de Malvinas el Concejo Deliberante de Tigre invita a los vecinos a un ciclo de actividades que comenzará este jueves 7 de abril a las 18 Hs. con una charla del coronel Esteban Vilgre Lamadrid sobre su experiencia en el conflicto armado._

Del evento participarán además veteranos de Tigre que contarán los tiempos vividos hace 40 años en las islas.

Las actividades continuarán el jueves 21 del corriente a las 18 Hs. con la proyección de la película, El visitante, dirigida por Javier Olivera; y culminarán el jueves 28 de abril con la charla del ex combatiente de Malvinas Waldemar Aquino.

Las actividades se llevarán a cabo en las instalaciones del Concejo Deliberante de Tigre y son con entrada libre y gratuita.