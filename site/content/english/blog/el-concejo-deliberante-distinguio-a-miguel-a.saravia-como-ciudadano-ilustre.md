+++
bg_image = "/images/292063434_405667881598532_3854784225858388271_n.jpg"
date = 2022-07-13T03:00:00Z
description = ""
image = "/images/292063434_405667881598532_3854784225858388271_n-1.jpg"
title = "EL CONCEJO DELIBERANTE DISTINGUIÓ A MIGUEL A. SARAVIA COMO CIUDADANO ILUSTRE"
type = "post"

+++
_El Concejo Deliberante de Tigre en la octava sesión ordinaria realizó el reconocimiento del vecino de General Pacheco Miguel A. Saravia “Esta es una merecida distinción a la trayectoria de un hombre que forma parte importante de nuestra cultura, su instituto es un icono en el distrito y el Ballet de Arte Folclórico Argentino que dirige, es reconocido a nivel mundial por su excelencia representando a Tigre. Son los ejemplos a imitar” indicó Segundo Cernadas tras entregar el reconocimiento._

Antes de la entrega del galardón la presidente del bloque Juntos y vecina de General Pacheco Ana María Fernández Costa realizó una reseña de la vida de Miguel Ángel Saravia, destacándolo como un actor importante no solo de la cultura sino de la comunidad y las instituciones de Pacheco. También destacó su calidad humana y los valores que se enseñan en el instituto de danzas que representa un orgullo para todos los vecinos de Tigre. “Todas estas acciones, su enorme aporte a la cultura y difusión a las tradiciones lo hacen merecedor de este premio” concluyó.

En ese mismo sentido la concejala Gisela Zamora expresó palabras de agradecimiento para Saravia y remarcó “Cada vez que el Ballet de Arte Folclórico Argentino sale a recorrer el mundo nos representan de la mejor manera, este es un merecido premio al trabajo”.

Luego del homenaje, ya en el trabajo legislativo, ingresaron 17 nuevos proyectos, entre los que se encuentran varios vinculados a la seguridad del distrito, el pedido de una nueva amarra isleña en la localidad de Dique Lujan, la solicitud de instalación de un cajero automático en la localidad de Troncos del Talar y el pedido de un cruce peatonal en Av. Santa María a la altura de Nordelta entre otras iniciativas.

Finalmente, los concejales dieron tratamiento y aprobación a 26 nuevas iniciativas para mejorar la vida de los vecinos de Tigre entre las que se encuentran la creación de un centro de primera infancia en el barrio Enrique Delfino de General Pacheco, La instalación de una cámara de seguridad en la zona de la estación Benavidez, la colocación de una parada de colectivos en Pacheco y la construcción de un monumento a los Cascos Azules.