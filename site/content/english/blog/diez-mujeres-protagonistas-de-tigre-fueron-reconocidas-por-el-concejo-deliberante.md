+++
author = ""
bg_image = "/images/img_0385-02.jpeg"
categories = []
date = 2021-03-08T20:00:00Z
description = "En el marco del Día Internacional de la Mujer, el Concejo Deliberante reconoció a mujeres protagonistas de Tigre seleccionadas por los vecinos mediante las redes sociales."
image = "/images/img_0385-02.jpeg"
tags = []
title = "DIEZ MUJERES PROTAGONISTAS DE TIGRE FUERON RECONOCIDAS POR EL CONCEJO DELIBERANTE"
type = "post"

+++
**_“Hoy es un día para acompañar y dar visibilidad a la lucha de las mujeres. Es un día de libertad y reconocimiento de derechos ”, dijo Segundo Cernadas, presidente del Concejo Deliberante de Tigre._**

En el marco del Día Internacional de la Mujer, el Concejo Deliberante reconoció a mujeres protagonistas de Tigre seleccionadas por los vecinos mediante las redes sociales.

Durante el encuentro las concejalas y concejales destacaron la lucha de las mujeres en su participación diaria por lograr que Tigre sea un partido más igualitario y justo. Particularmente se destacó el trabajo que las mujeres realizaron durante la pandemia. Al respecto, se pronunció el presidente del Concejo Deliberante, Segundo Cernadas, quien comenzó su presentación destacando que _“el 8 de marzo es una fecha de acompañamiento, lucha y visibilidad por los derechos de las mujeres y por los reclamos de justicia y mayor compromiso del Estado ”._

La concejala Sofía Bravo, impulsora del encuentro, describe que _“trabajamos para reconocer a todas las mujeres tigrenses por su labor en el día a día y buscamos darle visibilidad a la lucha que llevan a cabo para lograr un país con más igualdad en el acceso a derechos. Es un día también muy triste porque recordamos a todas las mujeres víctimas de violencia de género. Sobre todo durante la pandemia ”._

El reconocimiento surgió a partir de una consulta de Segundo Cernadas y otros concejales en redes sociales. En la misma invitaron a _"todas y todos los vecinos de Tigre a proponer aquellas mujeres que destacaron en distintos sectores de nuestra sociedad como pueden ser los clubes, las instituciones, las organizaciones barriales, los partidos políticos, los centros de salud, etc."_

De un total de 41 mujeres referentes en Tigre, propuestas por los vecinos del partido, se eligieron a las 10 candidatas finalistas, quienes recibieron un diploma y un presente a modo de agradecimiento por su labor.

_“Es un momento del año en donde buscamos dar a conocer a la sociedad el gran trabajo que hacen estas mujeres para la comunidad. Suele pasar que en muchos casos ellas son reconocidas únicamente en su barrio, pero se tiene que saber públicamente lo que hacen. Por eso es bueno que se les dé el espacio que realmente merecen y que la gente pueda apreciar su trabajo ”, destacó Cernadas._

#### LAS CANDIDATAS SELECCIONADAS

 1. **Silvia Cívico** , es médica del Pami, especialista en cirugía y médica clínica del hospital de Pacheco. Durante la pandemia, debido a la escasez de recursos en el Hospital (no les daban guantes ni equipo de protección personal), tuvo que alquilarse un departamento para no contagiar a su familia, por la falta de cuidados y su alta exposición al contagio. Durante el encuentro destacó que _“Todavía sigue siendo muy difícil ser mujer”_
 2. **Leticia Vanesa Diaz** , es Jefa de prefectura en Dique Lujan. Es Licenciada en Psicología y especialista en inteligencia criminal: _“agradezco a todas las mujeres de tigre por su lucha y trabajo día a día. Este premio es para todas ustedes ”,_ destacó Díaz
 3. **Maria Cristina Juárez** , es emprendedora y está a cargo del “Taller del abuelo”, en donde fabrican juguetes de madera y tejidos para los chicos. Hoy día venden por mercado libre y realizan envíos a todo el país. _“No tengo más que palabras de agradecimiento, trabajo siempre con una sonrisa porque lo que hago es para los chicos”._
 4. **Liliana Petrei:** Es maestra de primaria de la Escuela N ° 6 de Tigre. _"El 2020 fue un año difícil educación, tuvimos que trabajar todo el día, haciendo de nexo entre el colegio y las familias para que los chicos podrían tener una buena"._
 5. **Maria Soledad Armas** , Es estudiante de enfermería y vacunadora de “Campaña COVID-19” en el Hospital Materno Infantil y en el Hospital de Agudos. Tigre-Pacheco. _“Vivimos momentos muy angustiantes durante el 2020 y por eso es tan gratificante cuando la gente recibe la vacuna, porque lo ve como una esperanza de vida”._
 6. **María Graciela** , madre superiora del Hogar de Mujeres con discapacidad Santa Rosa de Tigre. _"No hay que tener miedo de acercar a Dios a la gente y cuidarlos con amor y cariño"_
 7. **Helen Larraburu** , lleva adelante una cooperativa de recolección de residuos en el Ahorcado “La voz de los trabajadores” y el merendero Manitos en Rincón de Milberg. _"Trabajamos muy duro para devolverle a las mujeres la posibilidad de tener un trabajo digno"_
 8. **Sofía Eugeni** , pintora, ilustradora y muralista. En su Tigre está siempre presente y lo refleja con un realismo que apoya a mostrar su belleza: _“Trabajo de lo que me gusta y me apasiona y eso me hace muy feliz”._
 9. **Maria Alejandra Zapata** , lleva adelante el Centro Cultural Almirante Brown en Barrio Garrote donde funciona un merendero y se brinda apoyo escolar así como contención. Asiste a más de 200 personas.
10. **Gisela Acevedo** , lucha por la igualdad social y trabaja con gran compromiso para quienes menos tienen. Todos los días cocina, brinda alimentos y ayuda a sus vecinos de Rincón de Milberg para lograr una mejor integración social.