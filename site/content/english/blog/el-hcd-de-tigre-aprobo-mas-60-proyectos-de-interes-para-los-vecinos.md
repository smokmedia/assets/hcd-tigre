+++
author = ""
bg_image = "/images/img_1162-01.jpeg"
categories = []
date = 2021-03-23T17:00:00Z
description = ""
image = "/images/img_1162-01.jpeg"
tags = []
title = "EL HCD DE TIGRE APROBÓ MÁS 60 PROYECTOS DE INTERÉS PARA LOS VECINOS"
type = "post"

+++
_Se declaró como zona de pesca deportiva a la calle Caupolicán, se solicitó el saneamiento del Arroyo El Claro, el desarrollo de una campaña de concientización para promover la donación de sangre y la adhesión al Programa de Formación en Desarrollo Sostenible, Ecología y Ambiente, entre otros._

Con la presencia de 20 Concejales y Concejalas, sobre un total de 24, este martes 23 de marzo se llevó a cabo la 2° Sesión Ordinaria del Honorable Concejo Deliberante de Tigre. La misma fue de manera presencial y respetando los protocolos sanitarios establecidos.

Hubo un sentido homenaje a las víctimas y a los detenidos y desaparecidos de la última dictadura militar.

En esta primera sesión ordinaria del año fueron aprobados más de 60 proyectos e ingresaron 80 nuevos proyectos, que fueron girados a comisión para ser tratados durante los próximos días.

**_ALGUNOS DE LOS PROYECTOS APROBADOS_**

Se requirió la reparación de aceras y calzadas en distintos barrios del distrito, así como también la higienización urbana, el entubamiento de zanjas y la colocación de contenedores móviles de residuos, entre otros

Declararon de Interés Legislativo Municipal el Programa “Ganar-Ganar” promovido por la Organización de las Naciones Unidas Mujeres, la Organización Internacional del Trabajo y la Unión Europea.

Impulsaron una campaña de concientización tendiente a promover la donación de sangre en el marco del Aislamiento Social, Preventivo y Obligatorio.

Además, se solicitó la adhesión al Programa de Formación en Desarrollo Sostenible, Ecología y Ambiente” (Ley Yolanda).