+++
author = ""
bg_image = "/images/whatsapp-image-2022-03-22-at-5-04-32-pm.jpeg"
categories = []
date = 2022-03-23T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-03-22-at-5-04-32-pm.jpeg"
tags = []
title = "EL CONCEJO DELIBERANTE DE TIGRE HOMENAJEÓ A LAS VÍCTIMAS DE LA INVASIÓN RUSA A UCRANIA"
type = "post"

+++
_El Concejo Deliberante de Tigre realizó un minuto de silencio en el comienzo de la primera sesión ordinaria del año. El presidente del cuerpo, Segundo Cernadas, solicitó a los presentes ponerse de pie y realizar un minuto de silencio para recordar a las víctimas de la guerra por la invasión rusa a Ucrania._

Hace pocos días, con la visita del diplomático Jorge Faurie al Concejo Deliberante, Cernadas había condenado la guerra y llamado a tener una posición clara en el país a favor de la paz.

“Nos preocupa lo que está sucediendo en Ucrania, nos preocupan las consecuencias de esta guerra y es muy importante que tomarnos este tiempo de reflexión por las víctimas del enfrentamiento armado en tierra ucraniana” indicó Segundo Cernadas concluida la sesión.