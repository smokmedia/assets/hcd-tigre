+++
bg_image = "/images/whatsapp-image-2022-12-01-at-14-51-32.jpeg"
date = 2022-12-01T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-12-01-at-14-51-32.jpeg"
title = "DIAGNOSTICO Y DESAFIOS PARA LA GESTIÓN EDUCATIVA EN TIGRE"
type = "post"

+++
El próximo 7 de diciembre a las 17 horas se realizará la presentación, en las instalaciones del Concejo Deliberante, del informe realizado por la facultad de economía de la UBA (Universidad de Buenos Aires) respecto del diagnóstico y desafíos de la gestión educativa local en Tigre.

Los disertantes serán el doctor en ciencias económicas Omar Quiroga, Director del Centro de Ciudades Inteligentes de la UBA, el contador René Escobar Director del programa y la Licenciada Ana Romina Sarmiento Coordinadora del programa.

Cabe destacar que a principio de año el Concejo Deliberante de Tigre firmó un convenio con la Facultad de economía de la Universidad de Buenos Aires para trabajar en este informe sobre la situación educativa del distrito y las potencialidades que tiene Tigre en esa materia.