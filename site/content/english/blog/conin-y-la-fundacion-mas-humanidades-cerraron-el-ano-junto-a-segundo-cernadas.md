+++
bg_image = "/images/whatsapp-image-2022-12-06-at-14-08-17.jpeg"
date = 2022-12-05T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-12-06-at-14-08-17.jpeg"
title = "CONIN Y LA FUNDACIÓN MÁS HUMANIDADES CERRARON EL AÑO JUNTO A SEGUNDO CERNADAS"
type = "post"

+++
_Participaron en el HCD Tigre mamás incluidas en el programa de nutrición de la prestigiosa Fundación Conin que realizan los talleres de formación laboral impulsados por distintas fundaciones y ONGs en el partido de Tigre._

“Para nosotros es muy importante el trabajo que realizan las fundaciones Más Humanidades y Alas entre otras tantas, para complementar la nutrición de los niños con una salida laboral para sus mamás, tenemos que seguir dando oportunidades a todas aquellas familias de Tigre que quieren salir adelante trabajando” indicó Segundo Cernadas.

El encuentro, que sirvió como clausura de los talleres de formación laboral, incluyó una muestra de productos que realizaron las mamás en las diferentes actividades durante el año, tales como mermeladas, productos de las huertas orgánicas, adornos, cuadros, entre otros.

Los chicos también fueron parte importante del evento, mientras sus mamás participaban de la charla final de los talleres y de la muestra, los niños interactuaron en un entretenido show de magia, recibieron a un divertido payaso y participaron de juegos organizados en el hall del Concejo Deliberante.

“Debemos destacar el trabajo de tantas voluntarias que dictan las clases, de tantas ONGs que tienen un compromiso por aquellos que más necesitan y de todas las personas que forman parte de nuestro equipo y llevan adelante estas capacitaciones que buscan un futuro mejor para cada una de las familias que participan” concluyó el presidente del Concejo Deliberante de Tigre Segundo Cernadas.