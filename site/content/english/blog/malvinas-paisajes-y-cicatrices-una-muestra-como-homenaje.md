+++
author = ""
bg_image = "/images/whatsapp-image-2022-05-04-at-9-41-11-am.jpeg"
categories = []
date = 2022-05-04T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-05-04-at-9-41-11-am.jpeg"
tags = []
title = "“MALVINAS PAISAJES Y CICATRICES” UNA MUESTRA COMO HOMENAJE"
type = "post"

+++
_En Concejo Deliberante de Tigre quedó oficialmente inaugurada la muestra “Malvinas Paisajes y Cicatrices” de los fotógrafos Diego Arranz y Sergio Sierra. La misma retrata los vestigios hoy en día, que quedaron del conflicto en el Atlántico Sur, así como también, los impactantes paisajes de las Islas Malvinas con tomas realizadas durante un safari fotográfico._

“Malvinas es todo el año” expresó Diego Arranz, “Con esta muestra queremos dejar nuestra huella en el Concejo Deliberante de Tigre y que los vecinos vean la realidad de Malvinas hoy a cuarenta años del conflicto bélico”.

Por su parte Sergio Sierra indicó que “Las fotos muestran el armamento de la guerra, los utensilios que usaban para comer, la ropa y de alguna manera es un homenaje a nuestros héroes de Malvinas”

El evento fue presentado por el Vicepresidente del Concejo Fernando Matelli y contó con la presencia de los concejales Victoria Etchart y Rodrigo Molinos, veteranos de Malvinas, instituciones invitadas y vecinos.