---
title: TIGRE APROBÓ EL PRESUPUESTO PARA EL 2021
date: 2020-12-22T02:30:00.000+00:00
bg_image: images/backgrounds/panoramatigre.webp
description2: Aquí va una descripción de la noticia.
image: "/images/whatsapp-image-2020-12-21-at-22-56-27.jpeg"
categories: []
tags: []
type: post

---
Por unanimidad el Concejo Deliberante de Tigre aprobó el presupuesto municipal de gastos y la ordenanza fiscal que establece los ingresos para el próximo año.

Con la presencia de 23 concejales y 24 mayores contribuyentes, el Honorable Concejo Deliberante de Tigre aprobó por unanimidad la ordenanza fiscal y tributaria que reglamenta la recaudación del municipio para el próximo año.

El presupuesto para el año **2021** será de **$ 16.588.017.052** . Este monto representa un aumento del 61% respecto al presupuesto aprobado durante el 2019 que fue de $ 10.132.898.982 para el ciclo 2020, año en el cual se terminó con una ejecución total de $ 13.893.626.009.

De esta manera, los concejales de ambos espacios políticos, con la presencia de los mayores contribuyentes, sancionaron la ordenanza fiscal e impositiva y la solicitud de asistencia financiera a la Provincia de Buenos Aires.

A través de la aprobación de estas ordenanzas, se autorizó un 17% del aumento de las tasas municipales para el año entrante y hasta un 13% de aumento sujeto a la evolución de la inflación.

**La distribución del presupuesto aprobada luego de la asamblea con mayores contribuyentes resultó aprobada se distribuirá de acuerdo al siguiente criterio para cada secretaría:**

La Secretaría de Gobierno dispondrá de $ 379.766.340.

La Secretaría de Economía y Hacienda contará con un presupuesto de $ 1.299.488.682- resultando el que más se redujo respecto al presupuesto 2020.

La Secretaría de Obras y Servicios Públicos dispondrá de $ 8.944.521.288 resultando el importe que más creció respecto al año anterior y el que más presupuesto tuvo lugar.

La Secretaría de Salud dispondrá de $ 3.302.119.432.

La Secretaría de Desarrollo Económico, Participación y Relaciones con la Comunidad tendrá un presupuesto de $ 183.260.947

La Secretaría de Turismo contará con un presupuesto de $ 141.113.178

La Secretaría de Protección Ciudadana tendrá un presupuesto de $ 1.593.804.499

La Secretaría de Desarrollo Social y Políticas de Inclusión tendrá un presupuesto de $ 637.140.897

Tigre 21/12/20