+++
author = ""
bg_image = "/images/img_2202.jpg"
categories = []
date = 2021-06-22T03:00:00Z
description = ""
image = "/images/img_2202.jpg"
tags = []
title = "PRIMERA SESIÓN MIXTA DEL CONCEJO DELIBERANTE DE TIGRE"
type = "post"

+++
**_El Concejo Deliberante de Tigre presentó la primera Sesión Mixta, virtual y presencial, para que los concejales que deban mantenerse en aislamiento puedan participar de los encuentros._**

Con la participación de once concejales presentes en el recinto y dos de modo virtual, se llevó adelante la séptima reunión que desde este año se transmite de modo on line.

Con el objetivo de cumplir los protocolos sanitarios se permitirá que aquellos ediles que deban mantenerse en aislamiento, puedan continuar participando de los encuentros de poder y de las votaciones para seguir desempeñando sus funciones con normalidad.

La sesión comenzó con un sentido homenaje a la figura del General Manuel Belgrano por el día de la bandera y un emotivo saludo a todos los padres Tigrenses por el “Día del Padre”.

Fueron aprobados más de 40 proyectos durante la sesión e ingresaron otros 27 proyectos, que serán girados a comisión para ser tratados durante la semana.