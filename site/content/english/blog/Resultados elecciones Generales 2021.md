+++
author = ""
bg_image = "/images/img_7133.jpg"
categories = []
date = 2021-11-18T03:00:00Z
description = ""
image = "/images/img_7133.jpg"
tags = []
title = "LUEGO DEL ESCRUTINIO DEFINITIVO, EL HCD QUEDÓ EN PARIDAD ENTRE AMBOS BLOQUES"
type = "post"

+++
##### **Este jueves 18 de noviembre, finalizó el escrutinio definitivo de las elecciones en Tigre, sin arrojar diferencias en los resultados del escrutinio provisorio realizado en la jornada electoral del pasado domingo.**

Fuentes presentes en el escrutinio en la ciudad de La Plata informaron que la coalición "Juntos" se impuso en la elección a nivel local y contará con el ingreso de 7 concejales el próximo 10 de diciembre al Concejo Deliberante. Por su parte la coalición del Frente de Todos obtuvo 5 ediles.

Con estos resultados, quienes ingresarán al HCD por **_Juntos_** serán **Segundo Cernadas**, **Marcela Cesare**, **Adolfo Leber**, **Ximena Pereyra**, **Nicolás Massot**, **Josefina Pondé** y **Lisandro Lanzón**.

Por el **_Frente de Todos_**, los cinco ediles que entran son **Gisela Zamora**, **Francisco Fernández**, **Sandra Rossi**, **Alejandro Ríos** y **Victoria Etchart**.

De esta manera, la composición final del Concejo Deliberante de Tigre quedará con una paridad de 12 concejales para el Frente de Todos y 12 para Juntos.