+++
author = ""
bg_image = "/images/whatsapp-image-2022-02-23-at-18-53-38-1.jpeg"
categories = []
date = 2022-02-24T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-02-23-at-18-53-38-1.jpeg"
tags = []
title = "EL CONCEJO DELIBERANTE DE TIGRE SUMÓ A LA UBA PARA TRABAJAR EN UN PROGRAMA DE MEJORA PARA LOS VECINOS"
type = "post"

+++
El presidente del Concejo Deliberante de Tigre Segundo Cernadas y el decano de la Facultado de Ciencias Económicas de la UBA (Universidad de Buenos Aires) Ricardo Pahlen Acuña suscribieron un acuerdo de cooperación para evaluar problemáticas del distrito, realizar investigación y proponer soluciones concretas para los vecinos de Tigre.

El objetivo del convenio es que los docentes, el personal técnico, alumnos y egresados de la universidad que cursan posgrados puedan abordar temáticas vinculadas al desarrollo urbanístico, al sistema de salud, a la seguridad ciudadana, al tránsito y a una planificación de la ciudad a mediano y largo plazo.

“Tenemos que empezar a trabajar de una manera profesional y buscando la ayuda de los mejores, este acuerdo con la Facultad de Ciencias Económicas de la UBA es un paso importante en ese sentido, y nos va a permitir diagnosticar adecuadamente las demandas de Tigre y trabajar aportando desde el Concejo Deliberante soluciones inteligentes que mejoren la calidad de vida de los vecinos” indicó Segundo Cernadas tras la firma de convenio.

Por su parte el concejal Lisandro Lanzón que acompañó al presidente del HCD enfatizó “Es un orgullo que avancemos con una de las universidades públicas de mayor prestigio del país en el diseño de soluciones que los vecinos vienen esperando hace tantos años, este trabajo debe dar como resultado el crecimiento de cada localidad de Tigre.