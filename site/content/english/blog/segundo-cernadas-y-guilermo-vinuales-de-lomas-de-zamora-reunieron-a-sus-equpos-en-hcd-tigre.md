+++
bg_image = "/images/6623a656-2301-44ea-8dce-c2f35a9708b3.jpg"
date = 2022-12-02T03:00:00Z
description = ""
image = "/images/6623a656-2301-44ea-8dce-c2f35a9708b3-1.jpg"
title = "SEGUNDO CERNADAS Y GUILERMO VIÑUALES DE LOMAS DE ZAMORA REUNIERON A SUS EQUPOS EN HCD TIGRE"
type = "post"

+++
El presidente del Concejo Deliberante de Tigre Segundo Cernadas recibió a Guillermo Viñuales, ex jefe de gabinete del municipio de Lomas de Zamora. El objetivo de ambos fue poner el foco en una agenda de formación de equipos para gobernar mejor sus distritos.

"El desafío de Juntos es ganar el conurbano y gobernar mejor" dijo Segundo Cernadas y remarco: “Lo que más llama la atención, y atrae las miradas de la prensa, es como definimos las candidaturas, pero muchos dirigentes de la Provincia de Buenos Aires, de nuestro espacio, estamos trabajando primero para ganar y después para gobernar mejor” indicó tras la visita de Viñuales, referente de Juntos en Lomas de Zamora, a Tigre.

“Estuvimos reunidos compartiendo la experiencia de los equipos de Tigre, con Segundo somos de una misma generación, tenemos la misma vocación transformadora y creemos que hay que estar preparados para gobernar” detalló Guillermo Viñuales sobre la reunión mantenida con concejales de Tigre.

Durante la visita recorrieron las instalaciones del Concejo Deliberante, dialogaron sobre el manejo institucional del órgano deliberativo local y charlaron además sobre las expectativas que tienen en ambos distritos para la agenda de trabajo 2023.