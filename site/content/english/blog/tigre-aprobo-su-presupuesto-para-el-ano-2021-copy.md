+++
bg_image = "images/backgrounds/edificio4.jpg"
date = 2020-12-10T17:00:00Z
description = "El próximo 21 de diciembre se tratará en asamblea de concejales y vecinos mayores contribuyentes la ordenanza fiscal y a continuación los ediles tendrán a consideración el presupuesto enviado por el ejecutivo municipal."
image = "images/whatsapp-image-2020-12-11-at-10-12-03-1.jpeg"
title = "EL HCD DE TIGRE TRATA LA ORDENANZA FISCAL E IMPOSITIVA PARA EL 2021"
type = "post"

+++
**_El próximo 21 de diciembre se tratará en asamblea de concejales y vecinos mayores contribuyentes la ordenanza fiscal y a continuación los ediles tendrán a consideración el presupuesto enviado por el ejecutivo municipal._**

Este jueves 10 de diciembre por la tarde, con la presencia de 21 concejales, el titular del Cuerpo Segundo Cernadas, dio comienzo a la sesión ordinaria convocada para tratar la prórroga en el tratamiento de los proyectos de presupuesto y de ordenanza fiscal e impositiva para el 2021 junto a varios varios proyectos más.

La iniciativa fue votada por unanimidad y de esta manera se dió cumplimiento al paso previo que debe cumplir el Concejo Deliberante para realizar la asamblea durante la cual se considerará el monto y alcance de las tasas municipales y luego la aprobación o rechazo del presupuesto municipal. La cita será el próximo lunes 21 de diciembre en el Honorable Concejo Deliberante de Tigre.

A su vez los concejales dieron luz verde para el tratamiento de proyectos de resolución como la pavimentación de la calle Nuestra Señora de Luján, la eximición del pago de Tasas y Contribuciones al Parque de la Costa por el término de 2 años, el incremento en las medidas de seguridad en las calles Ombú y Triunvirato de la ciudad de Don Torcuato y en la Zona Sur de Tigre. Todos ellos deberán ser considerados previamente en comisión y en el caso del proyecto que exime de pago de tasas al Parque de la Costa se aprobó que sea tratado en la próxima reunión del cuerpo.

Homenajes

Entre los homenajes de la jornada, se mencionó el Día Internacional de los Derechos Humanos, recordando y conmemorando a las víctimas del proceso de reorganización militar. Además, los ediles conmemoraron la muerte de Diego Armando Maradona, fallecido el pasado miércoles 25 de noviembre. Algunos ediles aprovecharon para manifestar su apoyo o rechazo al proyecto de interrupción voluntaria del embarazo.