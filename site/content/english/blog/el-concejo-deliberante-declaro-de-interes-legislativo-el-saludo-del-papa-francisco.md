+++
bg_image = "/images/whatsapp-image-2022-10-14-at-09-48-48.jpeg"
date = 2022-12-08T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-10-14-at-09-48-48.jpeg"
title = "EL CONCEJO DELIBERANTE DECLARÓ DE INTERÉS LEGISLATIVO EL SALUDO DEL PAPA FRANCISCO"
type = "post"

+++
El Honorable Concejo Deliberante de Tigre declaró de interés legislativo municipal el cordial saludo de su Santidad el Papa Francisco al reverendo José Luis Quijano, párroco de la Inmaculada Concepción de Tigre, y a todos los fieles que participan de la procesión náutica con motivo de realizarse las celebraciones de María Inmaculada.

El Concejo Deliberante a través del expediente 371/2022 tomo conocimiento de la misiva enviada por el Papa para saludar a quienes asistan a la tradicional fiesta que se realiza en el distrito y con el acompañamiento unánime de los bloques políticos declaró de interés legislativo el saludo.

La nota además del saludo por las celebraciones, les pide a los fieles que mantengan vivas las tradiciones populares y que renueven la escucha atenta de la palabra y los sacramentos. Finalmente, el Santo Padre invocando la protección de la bienaventurada virgen María impartió para los fieles su bendición apostólica.

La declaración de interés legislativo municipal fue refrendada por el Presidente del HCD Tigre en el decreto 134/2022.