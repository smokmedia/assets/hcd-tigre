+++
bg_image = "/images/whatsapp-image-2022-07-04-at-7-19-33-pm.jpeg"
date = 2022-07-04T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-07-04-at-7-19-33-pm.jpeg"
title = "SEGUNDO CERNADAS: EL TURISMO DEBE SER CENTRAL EN EL DESARROLLO DE TIGRE"
type = "post"

+++
_“Necesitamos una verdadera cooperación entre el sector publico y el privado para generar desarrollo turístico. El turismo genera empleo y es una de las industrias que más crece en el mundo, tenemos que aprovechar la potencialidad del Delta, que es uno de los atractivos más lindos del país” indicó Segundo Cernadas en la apertura del encuentro realizado en el Concejo Deliberante._

Con la presencia del Presidente del Ente de Turismo de la Ciudad de Buenos Aires Lucas Delfino, se realizó en el Concejo Deliberante de Tigre un encuentro con prestadores turísticos locales para compartir estrategias para el desarrollo de la actividad a nivel local. El evento no solo permitió conocer el trabajo de la Ciudad DE Buenos Aires sino debatir problemáticas puntuales de Tigre para mejorar e impulsar a nuestro distrito en una actividad que puede crecer mucho más.

En la presentación de la actividad Segundo Cernadas destacó “Necesitamos gestión para que Tigre vuelva a ser una marca fuerte, trabajar para una convivencia armónica entre vecinos y turistas, y una verdadera cooperación entre el sector público y el privado para generar desarrollo turístico. El turismo genera empleo y es una de las industrias que más crece en el mundo, tenemos que aprovechar la potencialidad del Delta, que es uno de los atractivos más lindos del país”

“En CABA nosotros tenemos una agencia copiando el modelo de otras grandes ciudades del mundo, donde el sector público y privado hacen sinergia para posicionar a Buenos Aires como destino turístico en el exterior. Es importante que busquemos distintas alternativas para atraer turistas, nosotros visitamos cada provincia del país con nuestra oferta” compartió Lucas Delfino con los asistentes.

Luego de la exposición los presentes realizaron preguntas y compartieron muchas de las dificultades que encuentran para el desarrollo del turismo local, fundamentalmente vinculadas a las deficiencias del transporte fluvial, a la falta de conectividad en el Delta y a la falta de planificación y gestión en cada localidad del distrito.