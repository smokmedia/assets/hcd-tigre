+++
bg_image = "/images/whatsapp-image-2022-12-05-at-12-31-43.jpeg"
date = 2022-12-04T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-12-05-at-12-31-43.jpeg"
title = "EL CONCEJO DELIBERANTE DE TIGRE DISTINGUIÓ A PASTORES DE LA ZONA NORTE"
type = "post"

+++
“Para sacar a la Argentina, a la Provincia y a Tigre adelante tenemos que estar todos juntos, poniendo toda nuestra energía y trabajo por el lugar donde vivimos. Este es un reconocimiento a las iglesias y pastores por su aporte a la comunidad, es una distinción del Concejo Deliberante, de todos los bloques políticos, por la labor que realizan día a día” indicó Segundo Cernadas en el encuentro.

Varios fueron los motivos para celebrar, en primer lugar, la distinción a la Asociación de Pastores de la Zona Norte que se le entregó al pastor Luis Oscar Fernández presidente de la entidad, se declaró, además, la jornada como día de oración por el partido de Tigre. Finalmente se destacó la trayectoria de la Asociación que cumplió 35 años de servicio en la zona norte.

Luego del mensaje del pastor Fabián Villalba, Segundo Cernadas destacó la tarea del pastor Luis Osvaldo Yslas, vicepresidente de la entidad que nuclea a los pastores de la zona norte y de Olga María Moller, quien tiene su iglesia en Tigre hace más de 45 años “Estamos muy contentos de poder hacer este encuentro reconociendo el trabajo que hacen las iglesias en cada barrio de Tigre” concluyó el presidente del Concejo Deliberante.

Durante el encuentro realizó una oración por el partido de Tigre y por sus autoridades, además se hizo una breve reseña de la historia de la asociación de pastores y de las iglesias evangélicas en el país.

Estuvieron presentes los concejales Maximiliano Picco, Marcela Césare, Ximena Pereyra y Lisandro Lanzón.