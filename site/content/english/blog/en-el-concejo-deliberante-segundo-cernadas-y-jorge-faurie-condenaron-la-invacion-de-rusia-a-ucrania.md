+++
author = ""
bg_image = "/images/cernadas-y-faurie.jpeg"
categories = []
date = 2022-03-15T03:00:00Z
description = ""
image = "/images/cernadas-y-faurie.jpeg"
tags = []
title = "EN EL CONCEJO DELIBERANTE SEGUNDO CERNADAS Y JORGE FAURIE CONDENARON LA INVACIÓN DE RUSIA A UCRANIA"
type = "post"

+++
_El ex canciller organizador del G20, Jorge Faurie, estuvo invitado en el Concejo Deliberante de Tigre para analizar el contexto internacional y las relaciones exteriores del país: “En Argentina el gobierno no tiene un mapa del mundo actual. Estamos perdidos y cometimos un grave error al no condenar de entrada la invasión a Ucrania”, dijo el diplomático._

En el Concejo Deliberante de Tigre Jorge Faurie y Segundo Cernadas condenaron la invasión rusa a Ucrania y cuestionaron la política diletante del gobierno nacional en un encuentro con vecinos organizado por el concejal Mariano Pelayo.

Previo a la convocatoria Faurie y Cernadas conversaron sobre las oportunidades que tienen las ciudades que se abren al mundo y cómo atraer inversiones además de explotar de manera sustentable el turismo.

“En Argentina el gobierno no tiene un mapa claro del mundo actual. Estamos perdidos y cometimos un grave error al no condenar de entrada la invasión a Ucrania”, concluyó Fourie luego de enumerar la serie de errores y desatinos del gobierno nacional respecto de la guerra en Europa.

El diplomático hizo un poco de historia sobre la relación entre Ucrania y Rusia a través del tiempo, analizó el impacto que tendrá la guerra en el mundo, la importancia de las ciudades en disputa en el contexto geopolítico y destacó la valentía del pueblo ucraniano.

Respecto a la posición argentina el presidente del Concejo Deliberante Segundo Cernadas dijo que “hay que ser claros y condenar la invasión de Rusia a Ucrania. Argentina debe ser un país que trabaje por la paz en el mundo, no alcanza con declaraciones ambiguas para quedar bien con todos”.

Sobre el viaje del presidente Alberto Fernández a Rusia el ex ministro expresó que “no fue en un buen momento para la Argentina. Estuvo muy mal preparado, no se va de visita a un país a criticar a otro” y afirmó que el país “debió llevar una agenda focalizada para lograr objetivos de inversión o financiamiento”.