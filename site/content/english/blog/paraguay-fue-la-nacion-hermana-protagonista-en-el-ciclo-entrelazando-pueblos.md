+++
bg_image = "/images/dsc01770-2.jpg"
date = 2022-11-16T03:00:00Z
description = ""
image = "/images/dsc01770-2.jpg"
title = "Paraguay fue la Nación hermana protagonista en el ciclo \"Entrelazando Pueblos\""
type = "post"

+++
_La última edición del ciclo de actividades culturales que acerca a diferentes naciones a la casa del pueblo de Tigre, tuvo el honor de poder afianzar relaciones con la República del Paraguay y sus representantes diplomáticos._

La Jornada comenzó con una procesión náutica de la Virgen patrona de Caacupé, y las actividades continuaron con shows musicales, gastronómicos y de danza donde vecinos, vecinas y miembros de la comunidad paraguaya pudieron disfrutar de una hermosa tarde.

Con respecto a esta edición, el vicepresidente del Concejo Deliberante, Fernando Mantelli expresó: "Nos pone muy contentos y nos motiva ver el recibimiento de los vecinos y vecinas con los eventos de este ciclo. A su vez, en este caso, debo destacar el excelente entendimiento que tuvimos con el Jefe de Misión de la Embajada, Juan Ramón Cano Montanía desde el momento que comenzamos a planificación esto"

Durante el evento se contó con una intérprete de arpa, una clase de cocina, danza tradicional de Paraguay y una muestra del Folcklore argentino a cargo del Ballet Querencia Gaucha.

Acompañaron la jornada autoridades de la Embajada de Paraguay, la Concejala Victoria Etchart, el Concejal Rodrigo Molinos, el equipo de trabajo, vecinos y vecinas de Tigre