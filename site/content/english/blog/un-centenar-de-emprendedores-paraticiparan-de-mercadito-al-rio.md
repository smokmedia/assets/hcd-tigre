+++
bg_image = "/images/whatsapp-image-2022-06-29-at-7-21-03-pm-1.jpeg"
date = 2022-06-30T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-06-29-at-7-21-03-pm-1.jpeg"
title = "UN CENTENAR DE EMPRENDEDORES PARATICIPARÁN DE MERCADITO AL RÍO"
type = "post"

+++
_“Tenemos la gran oportunidad de mostrar la fuerza emprendedora de Tigre y todo el potencial que tenemos para salir adelante” destacó Segundo Cernadas ante un centenar de emprendedores que ya comprometieron su presencia en la feria Mercadito al Río que comenzará el próximo domingo 10 de julio en los Jardines del Concejo Deliberante de Tigre._

Los emprendedores estuvieron presentes en la reunión informativa en la que se realizó la selección de los que participaran en las ediciones del mes de julio y así poder mostrar en cada uno de los stands el trabajo de sus emprendimientos.

“Estamos convencidos que el camino para mejorar Tigre es el del trabajo, es no bajar los brazos, tenemos la gran oportunidad de mostrar la fuerza emprendedora de Tigre y todo el potencial que tenemos para salir adelante” destacó en el encuentro Segundo Cernadas.

La feria Mercadito al Río tiene como objetivo que en cada puesto se pueda mostrar el trabajo de distintos emprendimientos de vecinos de todas las localidades de Tigre. Además, habrá shows en vivo y Foodtrucks durante todo el evento.

Participaron de la reunión las concejalas Sofia Bravo, Ximena Pereyra, Marcela Césare, Josefina Pondé, los concejales Lisandro Lanzón y Mariano Pelayo y el Secretario del HCD Tigre Leonardo Güi