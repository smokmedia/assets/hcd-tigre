+++
author = ""
bg_image = "/images/046991.jpg"
categories = []
date = 2022-04-27T03:00:00Z
description = ""
image = "/images/046991.jpg"
tags = []
title = "EL CONCEJO DELIBERANTE ELIMINÓ EL PASE SANITARIO Y EXIME DE PATENTE A LOS VEHÍCULOS HÍBRIDOS MUNICIPALIZADOS"
type = "post"

+++
_El presidente del Concejo Deliberante de Tigre Segundo Cernadas, luego de la sesión indicó que “Tenemos que dejar atrás normativas sancionadas en la pandemia que ahora son una molestia para los vecinos, por eso eliminamos el “Pase Sanitairo” en el distrito y además aprobamos una iniciativa que exime del pago de patente a vehículos municipalizados híbridos, que sean amigables con el ambiente, debemos contribuir a bajar los niveles de emisiones de monóxido de carbono y trabajar para un desarrollo sustentable”._

En la tercera sesión ordinaria del Concejo Deliberante de Tigre se dio ingreso a 47 nuevas iniciativas presentadas por los distintos bloques y se aprobaron 67 ordenanzas y resoluciones trabajados previamente en las comisiones.

Entre los expedientes ingresados se destacan la colocación de un semáforo en la localidad de Don Torcuato, el pedido de instalación de un tótem de seguridad en Ricardo Rojas, la iluminación en calles de Dique Lujan, un proyecto para crear un centro para adolescentes, jóvenes y adultos con discapacidad y un expediente que solicita la prohibición de la tracción a sangre en el distrito.

Mas tarde los concejales trataron y aprobaron sesenta siete proyectos entre los que se destacan la colocación de juegos inclusivos en el Jardín N° 915 de Las Tunas, el entubamiento de zanjas en el barrio Baires, la instalación de nuevas luminarias en la calle Alexis Carrel en la localidad de Troncos del Talar, el proyecto de ordenanza eximiendo del pago de patentes a los vehículos autopropulsados por sistema hibrido y el entubamiento de zanjas en el barrio de Villa Liniers en Rincón de Milberg.

Cabe destacar que en la media hora de consultas y homenajes el concejal Juan María Furnari repudio la campaña realizada por el municipio de Morón con concejos para el uso de drogas “Debemos repudiar enérgicamente estas acciones que son contrarias al rol del estado, que debe combatir a los narcos y ayudar a los jóvenes que quieren salir de las drogas”.