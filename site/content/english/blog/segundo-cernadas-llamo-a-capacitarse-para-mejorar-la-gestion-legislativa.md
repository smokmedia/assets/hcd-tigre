+++
bg_image = "/images/whatsapp-image-2022-06-16-at-11-32-18-am.jpeg"
date = 2022-06-16T03:00:00Z
description = ""
image = "/images/whatsapp-image-2022-06-16-at-11-32-18-am.jpeg"
title = "SEGUNDO CERNADAS LLAMÓ A CAPACITARSE PARA MEJORAR LA GESTIÓN LEGISLATIVA"
type = "post"

+++
_“Estas capacitaciones tienen como objetivo mejorar el trabajo legislativo del Concejo Deliberante de Tigre, es muy importante que quienes trabajan en gestión se capaciten permanentemente para brindar mejores servicios a nuestros vecinos” indicó Segundo Cernadas._

El presidente del Concejo Deliberante Segundo Cernadas compartió junto a Diego Enrich, Director General de Asuntos Legislativos del Gobierno de la Ciudad Autónoma de Buenos Aires, de un encuentro en el que aportaron herramientas para el desempeño del poder legislativo. El taller fue abierto a la comunidad y orientado a bridar conocimientos para mejorar la labor de concejales, asesores y funcionarios.

“Es un gusto recibir a Diego (Enrich) en Tigre, es alguien a quien conozco desde hace tiempo y fue de mucha ayuda por su experiencia cuando comencé como concejal” indicó Segundo Cernadas en la apertura de la actividad y añadió “Estas capacitaciones tienen como objetivo mejorar el trabajo legislativo del Concejo Deliberante de Tigre, es muy importante que quienes trabajan en gestión se capaciten permanentemente para brindar mejores servicios a nuestros vecinos”.

Por su parte Diego Enrich destaco que “Es un gran desafío conocer los reglamentos y funcionamientos del Concejo Deliberante ya que eso nos permite impulsar las ideas de la forma más adecuada para transformarse en proyectos que beneficien a los vecinos”.

La actividad permitió conocer parte del funcionamiento de la legislatura de la Ciudad de Buenos Aires, algunos procesos de la Cámara de Diputados de la Nación y compararlos con el trabajo de los concejales en las comisiones del HCD Tigre. Además, los expositores permitieron la interacción permanente con los asistentes, contestando preguntas y compartiendo experiencias vividas en otros Concejos Deliberantes del conurbano bonaerense.