+++
author = ""
bg_image = "/images/279384003_355388299959824_6834244832585784217_n.jpg"
categories = []
date = 2022-04-28T03:00:00Z
description = ""
image = "/images/279384003_355388299959824_6834244832585784217_n-1.jpg"
tags = []
title = "UN ENCUENTRO PARA CONCIENTIZAR SOBRE EL CUIDADO DEL PLANETA"
type = "post"

+++
_El Presidente del Concejo Deliberante Segundo Cernadas participó de un encuentro para concientizar sobre el cuidado del ambiente junto a Adrián Gluck de @iloverunn (Organizador de eventos deportivos) y Majo Rutilo de la institución Zapatillas Verdes, una agrupación que realiza Eco Running._

“Tigre es un lugar maravilloso, donde la gente hace mucho deporte y nosotros tenemos la obligación de cuidarlo, no sólo por nosotros, sino por el planeta que le vamos a dejar a nuestros hijos, es triste ver como toneladas de botellas plásticas terminan en nuestros ríos del Delta” indicó Segundo Cernadas.

Luego Adrián Gluck contó su experiencia en la organización de eventos de running “En cada carrera veíamos al finalizar muchísima basura y empezamos a concientizar primero, y a penalizar luego, para solucionar el problema y contribuir al cuidado del lugar donde desarrollamos nuestras actividades, corremos en lugares maravillosos y cuando nos vamos deben quedar tal como los encontramos”.

Finalmente, Majo Rutilo, quien fue corredora profesional, relató cómo empezó su toma de conciencia por el cuidado del ambiente y como desde “Zapatillas Verdes” impulsan iniciativas simples para que todos podamos hacer nuestra parte “Empezamos levantando una “botellita” de agua y contagiando para que todos lo hagan y hoy llevamos más de nueve toneladas de plástico entregadas a recolectores urbanos para ser recicladas. Es empezar e involucrar a otros a hacerlo” concluyó