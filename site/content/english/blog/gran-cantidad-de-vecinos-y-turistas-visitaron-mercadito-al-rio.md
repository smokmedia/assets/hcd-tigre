+++
bg_image = "/images/293459353_606424360853969_5070395096058211829_n.jpg"
date = 2022-07-18T03:00:00Z
description = ""
image = "/images/293459353_606424360853969_5070395096058211829_n-1.jpg"
title = "GRAN CANTIDAD DE VECINOS Y TURISTAS VISITARON “MERCADITO AL RÍO”"
type = "post"

+++
_La nueva feria de emprendedores de Tigre Mercadito al Río abrió sus puertas “Más de cien emprendedores desmostaron de lo que somos capaces si no nos resignamos, el esfuerzo y el trabajo son el camino que tenemos para salir adelante, el potencial emprendedor de Tigre es una muestra de que podemos estar mucho mejor indicados” Segundo Cernadas._

La nueva feria que convocó a los vecinos y turistas contó con más de cien stands con productos fabricados por emprendedores de las distintas localidades del distrito, hubo gastronomía y shows en vivo para toda la familia durante todo el evento.

“Es muy importante que no bajemos los brazos, los emprendedores nos desmostramos en Mercadito al Río de lo que somos capaces si no nos resignamos, el esfuerzo y el trabajo son el camino que tenemos para salir adelante, el potencial emprendedor de Tigre es una muestra de que podemos estar mucho mejor” remarcó Cernadas

El presidente del Concejo Deliberante recorrió cada uno de los puestos que ofreció una variada gama de productos desde artículos de decoración, ropa, almohadones, juguetes, productos para mascotas, macetas, artículos de porcelana fría o velas aromáticas entre otras varias propuestas artesanales.

“Desde el Concejo Deliberante tratamos de impulsar iniciativas que mejoren la calidad de vida de los vecinos, la Feria Mercadito al río es una oportunidad para aquellos que creen como nosotros, que Tigre se pone en acción cuidando a los que apuestan por nuestra ciudad y oportunidades para los que quieren trabajar” concluyó el presidente de HCD Tigre.

La feria de emprendedores Mercadito al Río se realizará todos los domingos de 11 a 17 horas en los jardines del Concejo Deliberante de Tigre.