---
title: Educación
bg_image: images/Comisiones/educacion.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Ana María Fernándes Costa
- puesto: 'Vicepresidente:'
  nombre: Victoria Etchart
- puesto: 'Secretario:'
  nombre: Josefina Ponde
- puesto: 'Vocal:'
  nombre: Lisandro Lanzón
- puesto: 'Vocal:'
  nombre: Verónica Caamaño
- puesto: 'Vocal:'
  nombre: Gisela Zamora
- puesto: 'Vocal:'
  nombre: Fernando Mantelli

---
##### Día de Reunión: Miércoles, 12:00 pm.

### Funciones

a. Todo asunto relacionado con la Educación y la infraestructura educacional; reparaciones, adecuaciones, construcción de espectáculos educacionales destinados a tal fin en todos sus ciclos.


b. Todo lo vinculado con bibliotecas, espectáculos teatrales, cinematográficos, musicales, escuelas, universidades, museos, fiestas, homenajes y publicaciones.


c. Todo lo relativo a la educación en general y a la educación sexual en particular, a la venta y exposición de escritos o publicaciones pornográficas e inmorales, a la vagancia o prostitución, espectáculos obscenos y/o que ofendan la moral y las buenas costumbres.