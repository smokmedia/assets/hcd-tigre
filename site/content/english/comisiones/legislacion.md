---
title: Legislación
date: '2019-07-06T15:27:17.000+06:00'
bg_image: images/Comisiones/Legislacion.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Nicolás Massot
- puesto: 'Vicepresidente:'
  nombre: Micaela Ferraro
- puesto: 'Secretario:'
  nombre: Juan Furnari
- puesto: 'Vocal:'
  nombre: Sofía Bravo Adamoli
- puesto: 'Vocal:'
  nombre: Rodrigo Molinos
- puesto: 'Vocal:'
  nombre: Sandra Rossi
- puesto: 'Vocal:'
  nombre: Florencia Mosqueda

---
##### Día de Reunión: Miércoles, 09:00 am.

### Funciones

a. Informar y/o dictaminar sobre toda petición, asunto oficial o particular, presentado al concejo.

b. En los casos previstos en el artículo 63, Incisos 1 a 6 inclusive, de la Ley Orgánica de las Municipalidades.

c. Cuestiones reglamentarias o de interpretación.

d. Separación del Contador y Tesorero.

e. Obligaciones de los Escribanos en los asuntos de transmisión o gravpamenes de bienes.

f. Todo asunto que esté o no expresamente destinado a otra comisión por este reglamento o se tengan dudas.

g. Reformas o interpretación del Reglamento, así como respecto a Ordenanzas, disposiciones o leyes y todo otro tema que tenga relación con las mismas.

h. Cuestiones de privilegio.