---
title: Bienestar Social y Salud
bg_image: images/Comisiones/Hospital-tigre.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Micaela Ferraro
- puesto: 'Vicepresidente:'
  nombre: Sofía Bravo Adamoli
- puesto: 'Secretario:'
  nombre: Fernando Mantelli
- puesto: 'Vocal:'
  nombre: Gisela Zamora
- puesto: 'Vocal:'
  nombre: Josefina Ponde
- puesto: 'Vocal:'
  nombre: Marcela Césare
- puesto: 'Vocal:'
  nombre: Ximena Pereyra

---
##### Día de Reunión: Miércoles, 10:00 am.

### Funciones

a. Dictaminar y legislar sobre medicina preventiva, medicina y sanidad asistencial y acción social, higiene y nutrición.


b. Gobierno de los centros hospitalarios, centro asistenciales y de primeros auxilios. Servicio de ambulancias médicas.


c. Inspección de establecimientos insalubres, peligrosos e incómodos para el vecindario; las casas de inquilinato, mercado de abasto y tabladas.


d. Expendio y consumo de artículos alimenticios.


e. Epidemias en general, inspección veterinarias, crueldad con los animales e inspección de productos destinados al consumo.


f. Administración de las casas piadosas y de asilos.


g. Ubicación y funcionamiento de aparatos de altavoces anunciadores, letreros y publicidad, uso de radios y de equipos de audio por instituciones o particulares que influyan en la salud e integridad del vecindario.


h. Todo lo concerniente a los servicios fúnebres y sus colaterales y todo otro asunto relacionado con la salud pública.


i. Todo lo relacionado con la drogadicción en su faz social y con relación a terceros, su promoción y distribución