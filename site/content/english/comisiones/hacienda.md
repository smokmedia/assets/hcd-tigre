---
title: Hacienda
bg_image: images/Comisiones/hacienda-fondo.jpg
description: Conocé más sobre esta comisión
download_link: "#"
puesto:
- puesto: 'Presidente:'
  nombre: Rodrigo Molinos
- puesto: 'Vicepresidente:'
  nombre: Mariano Pelayo
- puesto: 'Secretario:'
  nombre: Rodrigo Alvarez
- puesto: 'Vocal:'
  nombre: Sandra Rossi
- puesto: 'Vocal:'
  nombre: Nicolás Massot
- puesto: 'Vocal:'
  nombre: Adolfo Leber
- puesto: 'Vocal:'
  nombre: Ana María Fernándes Costa
- puesto: 'Vocal:'
  nombre: Alejandro Ríos

---
##### Día de Reunión: Miércoles, 09:30 am.

### Funciones

a. Proponer y dictaminar sobre la Ordenanza Fiscal, Ordenanza Impositiva, Cálculo de Recursos y Presupuesto de Gastos de la Administración Municipal y sobre todo proyecto de reforma a los mismos.


b.Examinar las cuentas de la Administración Municipal; sueldos, asuntos o proyectos relativos.


c. Empréstitos, deuda pública, subvenciones, donaciones, subsidios, pensiones, administración y disposición de bienes municipales y todo otro asunto relacionado con las finanzas municipales.


d. Facultades que resulten de su materia específica otorgados por la Ley Orgánica de las Municipalidades al Concejo Deliberante.