+++
bg_image = ""
concejales = "Leonardo Güi"
date = 2021-12-22T03:00:00Z
description = ""
image = "/images/acuerdo-porcino-con-china-1024x768.jpg"
title = "UN NUEVO PARADIGMA AMBIENTAL"

+++
#### **La Ley Yolanda es una herramienta para combatir la desinformación, el desinterés y la falta de perspectiva ambiental en todas las políticas de Estado.**

La Cámara de Diputados de la Nación (en una jornada maratónica), finalmente convirtió en ley el proyecto que establece la formación integral en medio ambiente “para todas las personas que se desempeñen en la función pública en todos sus niveles y jerarquías”. El proyecto fue aprobado en general y en particular por 203 votos a favor, 1 en contra y 2 abstenciones.

Esta norma, formalmente llamada: “Programa de Formación para Agentes de Cambio en Desarrollo Sostenible”, también conocida como “Ley Yolanda”, es un hito en la historia del derecho administrativo y del propio derecho ambiental, un cambio de paradigma en la forma de abordar desde la administración pública, la problemática ambiental.

Es sincero (y merecido) el homenaje que se rinde a Yolanda Ortiz, quien fuera la primera Secretaria de Recursos Naturales y Ambiente Humano de la República Argentina en 1973.

Según el texto de la ley, se establece la “capacitación obligatoria en la temática de desarrollos sostenible y ambiente, para todas las personas que se desempeñen en la función pública en todos sus niveles y jerarquías”, teniendo en cuenta el enfoque educativo, el proyecto establece una “formación integral en perspectiva de desarrollo sostenible, apoyada en valores sustentables y ambientales para las personas que se desempeñen en la función pública”.

La Diputada Nacional Camila Crescimbeni es la autora del proyecto, y una de las principales impulsoras del debate medioambiental en el Congreso Nacional.

La “Ley Yolanda” es histórica en muchos aspectos. Se impone una capacitación transversal en materia ambiental sobre las personas que se desempeñen en la función pública, lo que representa un cambio de paradigma trascendental en cuanto a la formación de los funcionarios, tanto jerárquicos como administrativos. Es una herramienta para combatir la desinformación, el desinterés y la falta de perspectiva ambiental en todas las políticas de Estado.

Cabe mencionar, que en Argentina está vigente la Ley Nº 27.520 de “Presupuestos Mínimos de Adaptación y Mitigación al Cambio Climático Global”, esta norma establece la obligación de analizar el impacto ambiental de todas las políticas de los ministerios nacionales, a través del Gabinete Nacional de Cambio Climático.

La “Ley Yolanda” viene a incorporar una nueva forma de capacitación, y a formar parte de una nueva legislación ambiental en la República Argentina, buscando incorporar aspectos que subsanen la falta de reglamentación, y estar al nivel de lo que establece el Art. 41 de la Constitución Nacional.

Dentro de este marco, hay importantes proyectos a la espera de ser aprobados, como la Ley de Humedales, la modificación a la Ley General del Ambiente N° 25.675, que incorpora los principios “in dubio pro natura e in dubio pro aqua” (en caso de duda a favor de), el proyecto para modificar la Ley de Manejo del Fuego, que aborda una modificación de la Ley Nº 26.815, con especial énfasis en la prevención de incendios intencionales, y el proyecto de Presupuestos mínimos para la reducción progresiva de los plásticos de un solo uso, entre otros.

Este nuevo escenario demuestra el creciente interés de la población por la problemática ambiental, y un nuevo compromiso de la sociedad civil con el medio ambiente, en gran parte movilizada por los trágicos acontecimientos sufridos en Córdoba y en el Delta del Paraná, pero también con la fuerza de un cambio de época, en donde la Crisis Climática representa una realidad que no podemos seguir ignorando.

Es momento de tomar las riendas de un problema trascendental, la historia será la encargada de juzgar cómo nuestros líderes reaccionaron en un momento crucial de nuestra especie.

Estemos a la altura de las circunstancias.