---
title: Mercadito al Rio
activar: true
bg_image: images/backgrounds/tigre-mercado.jpeg
linkFormulario: https://bit.ly/3H3eqcM
botones:
- name: Formulario de inscripción
  link: https://bit.ly/3H3eqcM
- name: Ver bases y condiciones
  link: "/images/reglamento-me-final-1.pdf"
redes:
- name: "/images/icons/facebook.png"
  link: https://www.facebook.com/mercaditoalrio
- name: "/images/icons/correo-electronico.png"
  link: mailto:mercaditoalrio@gmail.com
- name: "/images/icons/instagram.png"
  link: https://www.instagram.com/mercaditoalrio/

---
#### Es un foro de emprendedores para visibilizar y promover los microemprendimientos y las distintas actividades de los vecinos de Tigre.

##### Requisitos para tener tu stand gratis:

###### - Poseer domicilio real en el municipio de Tigre.

###### - Los productos deberán tener un valor agregado (quedan excluidos productos de reventa).

##### Si querés reservar tu stand anótate en el siguiente [<span style="color: #38CE37"><b>Formulario</b></span>](https://bit.ly/3H3eqcM)