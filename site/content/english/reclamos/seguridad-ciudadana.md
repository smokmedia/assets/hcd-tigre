---
title: "Seguridad Ciudadana"
draft: false
# page title background image
bg_image: "images/Comisiones/seguridad-tigre.jpg"
# meta description
description : "Completá tus datos para iniciar tu solicitud "
# notice download link
download_link : ""
form: "https://formspree.io/f/xwkwrovb"
# type
type: "reclamos"
#procesos
tipos:
    - tipo : "Cámaras de vigilancia"

    - tipo : "Falta de presencia policial"

    - tipo : "Otros"
---


### Seguridad Ciudadana