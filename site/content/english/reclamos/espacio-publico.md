---
title: "Espacio Público"
draft: false
# page title background image
bg_image: "images/Comisiones/turismo-fondo.jpg"
# meta description
description : "Completá tus datos para iniciar tu solicitud "
# notice download link
download_link : ""
#link de formulario
form: "https://formspree.io/f/moqpkdjq"
# type
type: "reclamos"
#procesos
tipos:
    - tipo : "Bacheo y reparación de vias de circulación"

    - tipo : "Veredas y reparación de sendas peatonales"

    - tipo : "Poda"

    - tipo : "Alumbrado"

    - tipo : "Mantenimiento de plazas y paseos"

    - tipo : "Otros"
---


### Espacio Público