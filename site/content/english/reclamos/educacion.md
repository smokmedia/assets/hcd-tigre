---
title: "Educación"
draft: false
# page title background image
bg_image: "images/Comisiones/educacion.jpg"
# meta description
description : "Completá tus datos para iniciar tu solicitud "
# notice download link
download_link : ""
#link de formulario
form: "https://formspree.io/f/mvovblol"
# type
type: "reclamos"
#procesos
tipos:
    - tipo : "Infraestructura y mantenimiento de edificios"

    - tipo : "Servicios de asistencia alimentaria escolar"

    - tipo : "Otro"
---


### Educación