(function ($) {
  'use strict';

  // Preloader js    
  $(window).on('load', function () {
    $('.preloader').fadeOut(100);
  });

  // Sticky Menu
  $(window).scroll(function () {
    var height = $('.top-header').innerHeight();
    if ($('header').offset().top > 10) {
      $('.top-header').addClass('hide');
      $('.navigation').addClass('nav-bg2');
      $('.navigation').css('margin-top','-'+height+'px');
    } else {
      $('.top-header').removeClass('hide');
      $('.navigation').removeClass('nav-bg2');
      $('.navigation').css('margin-top','-'+0+'px');
    }
  });

  

  // Background-images
  $('[data-background]').each(function () {
    $(this).css({
      'background-image': 'url(' + $(this).data('background') + ')'
    });
  });

  //Hero Slider
  $('.hero-slider').slick({
    autoplay: false,
    autoplaySpeed: 7500,
    pauseOnFocus: false,
    pauseOnHover: false,
    infinite: false,
    arrows: false,
    fade: false,
    prevArrow: '<button type=\'button\' class=\'prevArrow\'><i class=\'ti-angle-left\'></i></button>',
    nextArrow: '<button type=\'button\' class=\'nextArrow\'><i class=\'ti-angle-right\'></i></button>',
    dots: false
  });
  $('.hero-slider').slickAnimation();

  // venobox popup
  $(document).ready(function () {
    $('.venobox').venobox();
  });

  // filter
  $(document).ready(function () {
    var containerEl = document.querySelector('.filtr-container');
    var filterizd;
    if (containerEl) {
      filterizd = $('.filtr-container').filterizr({});
    }
    //Active changer
    $('.filter-controls li').on('click', function () {
      $('.filter-controls li').removeClass('active');
      $(this).addClass('active');
    });
  });
  

  
  //  Count Up
  function counter() {
    var oTop;
    if ($('.count').length !== 0) {
      oTop = $('.count').offset().top - window.innerHeight;
    }
    if ($(window).scrollTop() > oTop) {
      $('.count').each(function () {
        var $this = $(this),
          countTo = $this.attr('data-count');
        $({
          countNum: $this.text()
        }).animate({
          countNum: countTo
        }, {
          duration: 1000,
          easing: 'swing',
          step: function () {
            $this.text(Math.floor(this.countNum));
          },
          complete: function () {
            $this.text(this.countNum);
          }
        });
      });
    }
  }
  $(window).on('scroll', function () {
    counter();
  });

  // Animation
  $(document).ready(function () {
    $('.has-animation').each(function (index) {
      $(this).delay($(this).data('delay')).queue(function () {
        $(this).addClass('animate-in');
      });
    });
  });


  // Log IN

//  var current = null;
//  document.querySelector('#email').addEventListener('focus', function(a) {
//  if (current) current.pause();
//  current = anime({
//    targets: 'path',
//    strokeDashoffset: {
//      value: 0,
//      duration: 700,
//      easing: 'easeOutQuart'
//    },
//    strokeDasharray: {
//      value: '240 1386',
//      duration: 700,
//      easing: 'easeOutQuart'
//    }
//  });
//  });
//  document.querySelector('#password').addEventListener('focus', function(a) {
//  if (current) current.pause();
//  current = anime({
//    targets: 'path',
//    strokeDashoffset: {
//      value: -336,
//      duration: 700,
//      easing: 'easeOutQuart'
//    },
//    strokeDasharray: {
//      value: '240 1386',
//      duration: 700,
//      easing: 'easeOutQuart'
//    }
//  });
//  });
//  document.querySelector('#submit').addEventListener('focus', function(a) {
//   if (current) current.pause();
//   current = anime({
//   targets: 'path',
//    strokeDashoffset: {
//      value: -730,
//      duration: 700,
//      easing: 'easeOutQuart'
//    },
//    strokeDasharray: {
//      value: '530 1386',
//      duration: 700,
//      easing: 'easeOutQuart'
//    }
//  });
// });

  // Concejales

//  $(document).ready(function(){
//    var zindex = 10;
    
//    $("div.card2").click(function(e){
//      e.preventDefault();
  
//      var isShowing = false;
  
//      if ($(this).hasClass("show")) {
//        isShowing = true
//      }
  
//      if ($("div.cards2").hasClass("showing")) {
        // a card is already in view
//        $("div.card2.show")
//          .removeClass("show");
  
//        if (isShowing) {
          // this card was showing - reset the grid
//          $("div.cards2")
//            .removeClass("showing");
//        } else {
          // this card isn't showing - get in with it
//          $(this)
//            .css({zIndex: zindex})
//            .addClass("show");
  
//        }
  
//        zindex++;
  
//      } else {
        // no cards in view
//        $("div.cards2")
//          .addClass("showing");
//        $(this)
//          .css({zIndex:zindex})
//          .addClass("show");
  
//        zindex++;
//      }
      
//    });
//  });
  
  

})(jQuery);



  
  
 